# Importing Libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Importing Dataset
dataset = pd.read_csv("vgsales.csv")
# Checking Size
len(dataset)
# Checking For Null Values
dataset.isnull().sum()
# Dropping Rows with an NaN record as Null Values Exist
dataset = dataset.dropna(axis = 0, subset = ["Year", "Publisher"])
# Description of the Dataset
dataset.info()


# Visualizing The Data

# Which Genre Is The Most Common
plt.figure(figsize = (10,10))
sns.countplot(x = dataset.Genre, order = dataset.Genre.value_counts().index[0:10])
plt.title("Which Genre Is the Most Common")

# Which Genre Has The Highest Global Sales
plt.figure(figsize= (10,10))
x = dataset.groupby('Genre')['Global_Sales'].agg('sum').sort_values(ascending = False)[0:10].index
y = dataset.groupby('Genre')['Global_Sales'].agg('sum').sort_values(ascending = False)[0:10]
sns.barplot(x=x,y=y)
plt.title("Which Genre Has The Highest Global Sales")

# Which Publisher Has The Most Published Games
plt.figure(figsize = (10,8))
sns.countplot(x = dataset.Publisher, order = dataset.Publisher.value_counts().index[0:5])
plt.title("Which Publisher Has The Most Published Games")

# Which Publisher Has The Highest Global Sales 
plt.figure(figsize = (20,10))
sns.barplot(x = dataset.groupby('Publisher')['Global_Sales'].agg('sum').sort_values(ascending = False)[0:10].index, y = dataset.groupby('Publisher')['Global_Sales'].agg('sum').sort_values(ascending = False)[0:10])
plt.title("Which Publisher Has The Highest Global Sales")

# Which Game Has The Highest Global Sales
highest_game_global_sales = dataset.sort_values(by = "Global_Sales",ascending = False)[:5]
plt.figure(figsize = (11, 5))
sns.barplot(x = highest_game_global_sales.Name, y = highest_game_global_sales.Global_Sales)
plt.title("Which Game Has The Highest Global Sales")

# Which Game Has The Highest NA_Sales
highest_game_na_sales = dataset.sort_values(by = "NA_Sales", ascending = False)[:5]
plt.figure(figsize = (11,5))
sns.barplot(x = highest_game_na_sales.Name, y = highest_game_na_sales.NA_Sales)
plt.title("Which Game Has The Highest North American Sales")

# Which Game Has The Highest European Sales
highest_game_european_sales = dataset.sort_values(by = "EU_Sales", ascending = False)[:5]
plt.figure(figsize = (11,5))
sns.barplot(x = highest_game_european_sales.Name, y = highest_game_european_sales.EU_Sales)
plt.title("Which Game Has The Highest European Sales")

# Which Game Has The Highest Sales In Japan
highest_game_japan_sales = dataset.sort_values(by = "JP_Sales", ascending = False)[:5]
plt.figure(figsize = (14,5))
sns.barplot(x = highest_game_japan_sales.Name, y = highest_game_japan_sales.JP_Sales)
plt.title("Which Game Has The Highest Japan Sales")

# Which Game Has The Highest Sales In The Remaining Regions
highest_game_other_sales = dataset.sort_values(by = "Other_Sales", ascending = False)[:5]
plt.figure(figsize = (11,5))
sns.barplot(x = highest_game_other_sales.Name, y = highest_game_other_sales.Other_Sales)
plt.title("Which Game Has The Highest Sales In The Remaining Region")
